package com.kensoftph.filechooser;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.scene.control.*;

import java.io.IOException;
import java.net.URL;


public class FileChooserController {


    private Stage stage; // Referencia al Stage principal
    private Scene scene; // Referencia a la escena principal



    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }



    @FXML
    void CambiarPagina(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("Form-Credit.fxml"));
            Parent root = loader.load();

            FormCredit controller = loader.getController();
            controller.setStage(stage);
            controller.setScene(scene);

            stage.setScene(new Scene(root)); // Cambiando la escena principal
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
