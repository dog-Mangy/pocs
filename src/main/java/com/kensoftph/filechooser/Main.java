package com.kensoftph.filechooser;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Main extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("file-chooser.fxml"));
        Scene scene = new Scene(fxmlLoader.load());

        // Obtener el controlador del FXMLLoader
        FileChooserController controller = fxmlLoader.getController();

        // Configurar las referencias al Stage y a la Scene en el controlador
        controller.setStage(stage);
        controller.setScene(scene);

        stage.setTitle("JavaFX FileChooser");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}
