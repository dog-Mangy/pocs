package com.kensoftph.filechooser;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class FormCredit implements Initializable {

    @FXML
    private TextField numeroTarjetaTextField;

    @FXML
    private TextField cedulaCiudadaniaTextField;

    @FXML
    private TextField NameAndLast;

    @FXML
    private TextField CodigoSeguridad;


    private Stage stage; // Referencia al Stage principal
    private Scene scene; // Referencia a la escena principal

    public static Alert errorMsg = new Alert(Alert.AlertType.ERROR);

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        validateIncome();
        validateLetters();
    }

    private void validateIncome() {
        numeroTarjetaTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                errorMessage("Solo ingrese números");
                numeroTarjetaTextField.setText(oldValue);
            }
        });

        cedulaCiudadaniaTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                errorMessage("Solo ingrese números");
                cedulaCiudadaniaTextField.setText(oldValue);
            }
        });
    }

    private void validateLetters() {
        NameAndLast.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("[a-zA-Z ]*")) { // Permite letras y espacios
                errorMessage("Solo ingrese letras");
                NameAndLast.setText(oldValue);
            }
        });
    }

    public static void errorMessage(String msg) {
        errorMsg.setTitle("Error");
        errorMsg.setContentText(msg);
        errorMsg.setHeaderText("Error Alert");
        errorMsg.showAndWait();
    }

    @FXML
    private void handleCompra() {
        String numeroTarjeta = numeroTarjetaTextField.getText();
        String nombreYApellido = NameAndLast.getText();
        String cedulaCiudadania = cedulaCiudadaniaTextField.getText();
        String claveSeguridad = CodigoSeguridad.getText();


        if (numeroTarjeta.isEmpty() || nombreYApellido.isEmpty() || cedulaCiudadania.isEmpty() || claveSeguridad.isEmpty()) {
            errorMessage("Todos los campos son obligatorios");
            return;
        }

        try (Connection conn = DatabaseConnection.getConnection()) {
            String query = "SELECT * FROM Datos WHERE NumeroTarjeta = ? AND NombreYApellido = ? AND CodigoSeguridad = ? AND CedulaCiudadania = ?";
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            preparedStatement.setInt(1, Integer.parseInt(numeroTarjeta));
            preparedStatement.setString(2, nombreYApellido);
            preparedStatement.setString(3, claveSeguridad);
            preparedStatement.setInt(4, Integer.parseInt(cedulaCiudadania));

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                // Datos coinciden, permitir la compra
                Alert successMsg = new Alert(Alert.AlertType.INFORMATION);
                successMsg.setTitle("Compra exitosa");
                successMsg.setHeaderText("Éxito");
                successMsg.setContentText("Los datos coinciden, compra realizada con éxito.");
                successMsg.showAndWait();
            } else {
                // Datos no coinciden
                errorMessage("Los datos ingresados no coinciden con nuestros registros.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
            errorMessage("Error al conectar con la base de datos.");
        }
    }
}
